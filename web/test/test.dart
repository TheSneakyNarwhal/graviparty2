import 'dart:math' as math;

import '../main.dart';


void main() {
  testRivenMath();
}

void testRivenMath() {
  const int runTimes = 50000000;
  RivenMath.init();
  Stopwatch sw = new Stopwatch()
    ..start();

  for (int i = 0; i < runTimes; i++) {
    num sin = math.sin(i);
    num cos = math.cos(i);

    if (i < 10) print('\tsin($i): $sin, cos($i): $cos');
  }

  print('dart math sin cos - $runTimes: ${sw.elapsedMilliseconds}ms');

  sw
    ..stop()
    ..reset()
    ..start();

  for (int i = 0; i < runTimes; i++) {
    num sin = RivenMath.sin(i);
    num cos = RivenMath.cos(i);

    if (i < 10) print('\tsin($i): $sin, cos($i): $cos');
  }

  print('RivenMath sin cos - $runTimes: ${sw.elapsedMilliseconds}ms');

  sw
    ..stop()
    ..reset()
    ..start();

  // ATAN2

  for (int i = 0; i < runTimes; i++) {
    num atan2 = math.atan2(i, i + 1);

    if (i < 10) print('\tatan($i): $atan2');
  }

  print('dart math atan2 - $runTimes: ${sw.elapsedMilliseconds}ms');

  sw
    ..stop()
    ..reset()
    ..start();

  for (int i = 0; i < runTimes; i++) {
    num atan2 = RivenMath.atan2(i, i + 1);

    if (i < 10) print('\tatan($i): $atan2');
  }

  sw.stop();

  print('RivenMath atan2 - $runTimes: ${sw.elapsedMilliseconds}ms');
}
