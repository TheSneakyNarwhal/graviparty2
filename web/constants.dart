part of graviparty;

abstract class Constants {
  static const int width = 640;
  static const int height = 480;


  static const List colors = const <int>[
    Color.Red,
    Color.Orange,
    Color.Yellow,
    Color.Green,
    Color.Blue,
    Color.Purple
  ];
}