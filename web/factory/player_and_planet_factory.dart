part of graviparty;

class PlayerAndPlanetFactory {
  static PlayerAndPlanetFactory _factory;

  static final Map players = <int, Player>{}; // <color, Player>
  static final Map planets = <int, Planet>{}; // <color, Planet>

  static void init() {
    if (_factory == null) _factory = new PlayerAndPlanetFactory._internal();
    print('init player and planet factory');
  }

  PlayerAndPlanetFactory._internal() {
    for (int i = 0; i < Constants.colors.length; i++) {
      var planet;
      var player;
      planet = new Planet(player, Planet.minRadius, Constants.colors[i]);
      player = new Player(planet, Constants.colors[i]);
      planets[Constants.colors[i]] = planet;
      players[Constants.colors[i]] = player;
    }
  }

  static void resetAll() {
    for (Planet planet in PlayerAndPlanetFactory.planets.values) {
      planet.removeFromParent();
    }

    for (Player player in PlayerAndPlanetFactory.players.values) {
      player.removeFromParent();
    }
  }
}
