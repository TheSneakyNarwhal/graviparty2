library graviparty;

import 'dart:html' as html;
import 'dart:math' as math;
import 'dart:async';

import 'package:stagexl/stagexl.dart';

part 'airconsole_manager/airconsole_manager.dart';
part 'arena/arena.dart';
part 'arena/default_arena.dart';
part 'factory/player_and_planet_factory.dart';
part 'input/keyboard.dart';
part 'math/riven_math.dart';
part 'math/rng.dart';
part 'sprites/arrow.dart';
part 'sprites/particle.dart';
part 'sprites/planet.dart';
part 'sprites/player.dart';
part 'sprites/recolorable_sprite.dart';
part 'sprites/wrap_sprite.dart';
part 'states/intro.dart';
part 'states/menu.dart';
part 'states/play.dart';
part 'states/stage_with_states.dart';
part 'states/state.dart';
part 'constants.dart';

void main() {
  RNG.init();
  Keyboard.init();
  RivenMath.init();
  PlayerAndPlanetFactory.init();
  AirConsoleManager.init();

  var canvas = html.querySelector('#stage');
//  var stage = new Stage(canvas)..backgroundColor = Color.Black;
  var stage = new StageWithStates(canvas)..backgroundColor = Color.Black;
  new RenderLoop().addStage(stage);

  stage
    ..addStates({'intro': new Intro(), 'menu': new Menu(), 'play': new Play()})
    ..startState('intro');
}
