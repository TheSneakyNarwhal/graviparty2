part of graviparty;

class Keyboard {
  static Keyboard _keyBoard;

  static final List _subscriptions = <StreamSubscription>[];
  static final List _heldDown = <int>[];

  static void init() {
    if (_keyBoard == null) _keyBoard = new Keyboard._internal();
    print('init keyboard');
  }

  Keyboard._internal() {
    html.document
      ..onKeyDown.listen((e) {
        if (!_heldDown.contains(e.keyCode)) _heldDown.add(e.keyCode);
      })
      ..onKeyUp.listen((e) {
        if (_heldDown.contains(e.keyCode)) _heldDown.remove(e.keyCode);
      });
  }

  static StreamSubscription onKeyDown(State state, int key, Function handler) {
    var sub = html.document.onKeyDown.listen((e) {
      if (e.keyCode == key && !state.paused) handler();
    });
    _subscriptions.add(sub);
    return sub;
  }

  static StreamSubscription onKeyUp(State state, int key, Function handler) {
    var sub = html.document.onKeyUp.listen((e) {
      if (e.keyCode == key && !state.paused) handler();
    });
    _subscriptions.add(sub);
    return sub;
  }

  static bool isKeyDown(int key) => _heldDown.contains(key);

  static void clearSubscriptions() {
    for (StreamSubscription sub in _subscriptions) {
      sub.cancel();
    }
    _subscriptions.clear();
  }
}
