part of graviparty;

class RNG {
  static RNG _rng;

  static int seed;

  static math.Random _random;

  static void init([int s]) {
    if (_rng == null) _rng = new RNG._internal(s);
    print('init rng');
  }

  RNG._internal(int s) {
    newSeed(s);
  }

  static bool nextBool() => _random.nextBool();

  static double nextDouble() => _random.nextDouble();

  static int nextInt(int max) => _random.nextInt(max);

  static void newSeed([int s]) {
    if (s == null) s = new math.Random().nextInt(1000000);
    seed = s;
    _random = new math.Random(seed);
  }
}
