part of graviparty;

class RivenMath {
  static RivenMath _rivenMath;

  // sin cos
  static const int _sinBits = 12;
  static const int _sinMask = ~(-1 << _sinBits);
  static const int _sinCount = _sinMask + 1;

  static const num _radFull = math.PI * 2.0;
  static const num _radToIndex = _sinCount / _radFull;
  static const num _degFull = 360.0;
  static const num _degToIndex = _sinCount / _degFull;

  static final List _sin = new List<num>(_sinCount);
  static final List _cos = new List<num>(_sinCount);

  // atan2
  static const int _atan2Bits = 7;
  static const int _atan2Bits2 = _atan2Bits << 1;
  static const int _atan2Mask = ~(-1 << _atan2Bits2);
  static const int _atan2Count = _atan2Mask + 1;
  static const int _atan2Dim = 128;

  static const num _invAtan2DimMinus1 = 1.0 / (_atan2Dim - 1);

  static final List _atan2 = new List<num>(_atan2Count);

  static void init() {
    if (_rivenMath == null) _rivenMath = new RivenMath._internal();
    print('init riven math');
  }

  RivenMath._internal() {
    // sin cos
    for (int i = 0; i < _sinCount; i++) {
      _sin[i] = math.sin((i + .5) / _sinCount * _radFull);
      _cos[i] = math.cos((i + .5) / _sinCount * _radFull);
    }

    for (int i = 0; i < 360; i += 90) {
      _sin[(i * _degToIndex).toInt() & _sinMask] = math.sin(i * math.PI / 180);
      _cos[(i * _degToIndex).toInt() & _sinMask] = math.cos(i * math.PI / 180);
    }

    // atan2
    for (int i = 0; i < _atan2Dim; i++) {
      for (int j = 0; j < _atan2Dim; j++) {
        _atan2[j * _atan2Dim + i] = math.atan2(j / _atan2Dim, i / _atan2Dim);
      }
    }
  }

  static num sin(num rad) => _sin[(rad * _radToIndex).toInt() & _sinMask];

  static num cos(num rad) => _cos[(rad * _radToIndex).toInt() & _sinMask];

  static num atan2(num y, num x) {
    if (y == 0 || y == 0.0) {
      return y;
    } else if (x == 0 || x == 0.0) {
      return 1.5707963267948966;
    }

    num add, mul;

    if (x < 0.0) {
      if (y < 0.0) {
        x = -x;
        y = -y;

        mul = 1.0;
      } else {
        x = -x;
        mul = -1.0;
      }
      add = -3.141592653;
    } else {
      if (y < 0.0) {
        y = -y;
        mul = -1.0;
      } else {
        mul = 1.0;
      }
      add = 0.0;
    }

    num invDiv = 1.0 / (((x < y) ? y : x) * _invAtan2DimMinus1);

    int xi = (x * invDiv).toInt();
    int yi = (y * invDiv).toInt();

    return (_atan2[yi * _atan2Dim + xi] + add) * mul;
  }
}
