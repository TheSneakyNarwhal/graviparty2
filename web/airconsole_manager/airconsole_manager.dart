part of graviparty;

class AirConsoleManager {
  static AirConsoleManager _manager;

  static Map colorToPlayer = <int, int>{};

  static List _usedColors = <int>[];

  static void init() {
    if (_manager == null) _manager = new AirConsoleManager._internal();
    print('init airconsole manager');
  }

  AirConsoleManager._internal() {
// TODO update whenever someone leaves

//  for all colors
    for (int i = 0; i < Constants.colors.length; i++) {
      colorToPlayer[Constants.colors[i]] = Constants.colors[i];
    }

//    debugTest();

    _updateUsedColors();
  }

  static void _updateUsedColors() {
    _usedColors =
        colorToPlayer.keys.where((k) => colorToPlayer[k] != null).toList();
  }

  static List getUsedColors() => _usedColors;

//  used to create random colored users
  static void debugTest() {
    for (int color in colorToPlayer.keys) {
      colorToPlayer[color] = null;
    }

    for (int i = 0; i < Constants.colors.length; i++) {
      int randomColor = Constants.colors[RNG.nextInt(Constants.colors.length)];
      colorToPlayer[randomColor] = randomColor;
    }

    _updateUsedColors();
  }
}
