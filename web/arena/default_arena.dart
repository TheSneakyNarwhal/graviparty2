part of graviparty;

class RegularArena implements Arena {
  State state;

  RegularArena(this.state);

  @override
  void create() {
    _placePlanets();
  }

  @override
  void update(num delta) {
    int moveVertical = 0;
    int moveHorizontal = 0;

    if (Keyboard.isKeyDown(html.KeyCode.W)) {
      moveVertical -= 2;
    }
    if (Keyboard.isKeyDown(html.KeyCode.S)) {
      moveVertical += 2;
    }
    if (Keyboard.isKeyDown(html.KeyCode.A)) {
      moveHorizontal -= 2;
    }
    if (Keyboard.isKeyDown(html.KeyCode.D)) {
      moveHorizontal += 2;
    }

    for (int color in AirConsoleManager.getUsedColors()) {
      PlayerAndPlanetFactory.planets[color]
        ..x += moveHorizontal
        ..y += moveVertical;
    }
  }

  @override
  void destroy() {
    PlayerAndPlanetFactory.resetAll();
  }

  void _placePlanets() {
    int retries = 0;
    List positions = <Point>[];
    List radii = <int>[];
    List colors = AirConsoleManager.getUsedColors();

    while (positions.length < colors.length && retries < 100) {
      int radius = RNG.nextInt(Planet.radiusVariation) + Planet.minRadius;
      Point pos = new Point(RNG.nextInt(Constants.width - radius * 2) + radius,
          RNG.nextInt(Constants.height - radius * 2) + radius);

      bool add = true;
      for (int i = 0; i < positions.length; i++) {
        Point otherPos = positions[i];
        int otherRadius = radii[i];

        if (new Point(otherPos.x, otherPos.y).squaredDistanceTo(pos) <
            math.pow(
                otherRadius + radius + Planet.minDistanceBetweenPlanets, 2)) {
          add = false;
          retries++;
          break;
        }
      }

      if (add) {
        positions.add(pos);
        radii.add(radius);
      }
    }

    int count = 0;
    for (int color in colors) {
      PlayerAndPlanetFactory.planets[color]
        ..x = positions[count].x + Constants.width / 2
        ..y = positions[count].y + Constants.height / 2
        ..radius = radii[count]
        ..addTo(state)
        ..setIndex(0);

      PlayerAndPlanetFactory.players[color]
        ..addTo(state)
        ..setIndex(0);

      count++;
    }
  }
}
