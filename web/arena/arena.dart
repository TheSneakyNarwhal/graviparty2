part of graviparty;

abstract class Arena {
  State state;

  void create();

  void update(num delta);

  void destroy();
}
