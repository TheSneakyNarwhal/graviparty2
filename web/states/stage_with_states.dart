part of graviparty;

class StageWithStates extends Stage {
  Map _states = <String, State>{};

  State currentState;

  StageWithStates(html.CanvasElement canvas) : super(canvas);

  void addState(String name, State state) {
    _states[name] = state;
    state.stage = this;
    print('added $name state');
  }

  void addStates(Map<String, State> states) {
    states.forEach((k, v) => addState(k, v));
  }

  void startState(String stateName) {
    if (currentState != null) {
      currentState
        ..pause()
        ..removeFromParent();
    }

    currentState = _states[stateName];
    addChild(currentState);
    currentState.start();

    print('start $stateName state');
  }
}
