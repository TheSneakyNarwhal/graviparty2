part of graviparty;

class Intro extends State {
  @override
  void create() {
    const String string = 'GPARTY';

    int count = 0;
    const int charWidth = Constants.width ~/ 6;
    var chars = <TextField>[];
    var format = new TextFormat('Oswald', 200, Constants.colors[0]);
    string.split('').forEach((s) {
      format.color = Constants.colors[count];
      var char = new TextField(s, format)
        ..width = charWidth
        ..height = Constants.height
        ..x = count * charWidth
        ..y = 125;
      addChild(char);
      chars.add(char);

      count++;
    });

    var tween = new Tween(this, 1.0, Transition.linear)
      ..animate.alpha.to(0)
      ..delay = 3.0
      ..onComplete = () => stage.startState('play');
    stage.juggler.add(tween);

    super.create();
  }
}
