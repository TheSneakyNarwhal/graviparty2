part of graviparty;

class Play extends State {
  Arena arena;

  List arrows = <Arrow>[];
  List particles = <Particle>[];

  @override
  create() {
    arena = new RegularArena(this)..create();

    Keyboard.onKeyDown(this, html.KeyCode.R, () {
      RNG.newSeed();
      AirConsoleManager.debugTest();
      arena
        ..destroy()
        ..create();
    });

    super.create();
  }

  @override
  update(num delta) {
    super.update(delta);

    arena.update(delta);
    debugText.text += '\nseed: ${RNG.seed}';
  }
}
