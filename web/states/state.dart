part of graviparty;

abstract class State extends DisplayObjectContainer {
  StageWithStates stage;

  TextField debugText;

  num _fpsAverage = null;

  bool _paused = true;
  bool get paused => _paused;

  EventStreamSubscription _stateEventSubscription;

  void create() {
    debugText = new TextField()
      ..defaultTextFormat = new TextFormat('Arial', 15, Color.White)
      ..width = 100
      ..visible = false
      ..addTo(this);

    Keyboard.onKeyDown(
        this, html.KeyCode.TILDE, () => debugText.visible = !debugText.visible);
  }

  void update(num delta) {
    if (_fpsAverage == null) {
      _fpsAverage = 1.00 / delta;
    } else {
      _fpsAverage = 0.05 / delta + 0.95 * _fpsAverage;
    }

    debugText
      ..text = '${_fpsAverage.round()} fps'
      ..text += '\n${children.length} sprites';
  }

  Future pause() {
    _paused = true;
    return _stateEventSubscription.cancel();
  }

  void resume() {
    _paused = false;
    _stateEventSubscription.resume();
  }

  void start() {
    if (_stateEventSubscription != null) {
      resume();
      return;
    }

    create();
    _stateEventSubscription = onEnterFrame.listen((e) => update(e.passedTime));
    _paused = false;
  }
}
