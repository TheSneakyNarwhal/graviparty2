part of graviparty;

class Particle extends RecolorableSprite implements Animatable {
  static final BitmapData _defaultBitmapData = _getDefaultBitmapData();

  static const num timeToLive = .9;

  num timePassed = 0;

  Particle(int color) : super(color, _defaultBitmapData.clone());

  @override
  advanceTime(num time) {
    timePassed += time;

    x %= Constants.width;
    y %= Constants.height;

//    TODO remove after time
//    if (timePassed > timeToLive) {
//      removeFromParent();
//    }
  }

  static BitmapData _getDefaultBitmapData() {
    var particle = new Shape()
      ..graphics.rect(1, 0, 2, 4)
      ..graphics.rect(0, 1, 4, 2)
      ..graphics.fillColor(Color.White)
      ..applyCache(0, 0, 4, 4);

    return new BitmapData.fromRenderTextureQuad(particle.cache);
  }
}
