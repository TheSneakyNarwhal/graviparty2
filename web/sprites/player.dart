part of graviparty;

class Player extends WrapSprite {
  static final BitmapData _defaultBitmapData = _getDefaultBitmapData();
  static final BitmapData _defaultMovingBitmapData =
      _getDefaultMovingBitmapData();

  static const int torsoWidth = 14;
  static const int torsoHeight = 10;
  static const int torsoOffset = -3;
  static const int headRadius = 11;
  static const int legWidth = 4;
  static const int legHeight = 4;

  final Planet planet;

  Player(this.planet, int color) : super(color, _defaultBitmapData.clone()) {
    pivotY = height;
  }

  @override
  advanceTime(num time) {
    // subtract 1 from planet radius so the feet look like they're touching the ground
    x = planet.x + (planet.radius - 1) * RivenMath.cos(rotation - math.PI / 2);
    y = planet.y + (planet.radius - 1) * RivenMath.sin(rotation - math.PI / 2);

    super.advanceTime(time);
  }

  static BitmapData _getDefaultBitmapData() {
    var shape = new Shape()
      // RIGHT LEG
      ..graphics.rect(headRadius + torsoWidth / 2 - legWidth,
          headRadius * 2 + torsoOffset + torsoHeight, legWidth, legHeight - 1)
      ..graphics.rect(
          headRadius + torsoWidth / 2 - legWidth + 1,
          headRadius * 2 + torsoOffset + torsoHeight + legHeight - 1,
          legWidth - 2,
          1)
      ..graphics.fillColor(Color.White)
      ..applyCache(0, 0, headRadius * 2,
          torsoHeight + headRadius * 2 + legHeight + torsoOffset);

    var legBitmap = new BitmapData.fromRenderTextureQuad(shape.cache);
    var bitmapDataMoving = _defaultMovingBitmapData.clone()
      ..drawPixels(legBitmap, legBitmap.rectangle, new Point(0, 0));
    return bitmapDataMoving;
  }

  static BitmapData _getDefaultMovingBitmapData() {
    var shape = new Shape()
      // HEAD
      ..graphics.beginPath()
      ..graphics.circle(headRadius, headRadius, headRadius)
      ..graphics.fillColor(Color.White)
      ..graphics.closePath()
      // TORSO
      ..graphics.beginPath()
      ..graphics.rect(headRadius - torsoWidth / 2, headRadius * 2 + torsoOffset,
          torsoWidth, torsoHeight)
      ..graphics.fillColor(Color.White)
      ..graphics.closePath()
      // LEFT LEG
      ..graphics.beginPath()
      ..graphics.rect(headRadius - torsoWidth / 2,
          headRadius * 2 + torsoOffset + torsoHeight, legWidth, legHeight - 1)
      ..graphics.rect(
          headRadius - torsoWidth / 2 + 1,
          headRadius * 2 + torsoOffset + torsoHeight + legHeight - 1,
          legWidth - 2,
          1)
      ..graphics.fillColor(Color.White)
      ..graphics.closePath()
      // LEFT EYE
      ..graphics.beginPath()
      ..graphics.rect(1 + headRadius / 2 - 4, headRadius - 1, 2, 4)
      ..graphics.rect(headRadius / 2 - 4, headRadius, 4, 2)
      // RIGHT EYE
      ..graphics.rect(1 + headRadius / 2 * 3, headRadius - 1, 2, 4)
      ..graphics.rect(headRadius / 2 * 3, headRadius, 4, 2)
      ..graphics.fillColor(Color.Black)
      ..applyCache(0, 0, headRadius * 2,
          torsoHeight + headRadius * 2 + legHeight + torsoOffset);

    return new BitmapData.fromRenderTextureQuad(shape.cache);
  }
}
