part of graviparty;

class WrapSprite extends RecolorableSprite implements Animatable {
  final List duplicates = new List<Bitmap>(3);

  num timePassed = 0;

  WrapSprite(int color, BitmapData bitmapData) : super(color, bitmapData) {
    for (int i = 0; i < 3; i++) {
      var bmp = new Bitmap(bitmapData)
        ..scaleX = scaleX
        ..scaleY = scaleY
        ..alpha = alpha
        ..pivotX = bitmapData.width / 2
        ..pivotY = bitmapData.height / 2;

      duplicates[i] = bmp;
    }

    pivotX = bitmapData.width / 2;
    pivotY = bitmapData.height / 2;
  }

  @override
  void addTo(State state) {
    state
      ..addChild(this)
      ..stage.juggler.add(this);

    for (Bitmap dup in duplicates) {
      state.addChild(dup);
    }
  }

  @override
  void removeFromParent() {
    if (parent != null) {
      for (Bitmap dup in duplicates) {
        parent.removeChild(dup);
      }

      parent
        ..stage.juggler.remove(this)
        ..removeChild(this);
    }
  }

  @override
  advanceTime(num time) {
    timePassed += time;

    x %= Constants.width;
    y %= Constants.height;

    duplicates[0]
      ..x = x < Constants.width / 2 ? x + Constants.width : x - Constants.width
      ..y = y;

    duplicates[1]
      ..x = x
      ..y = y < Constants.height / 2
          ? y + Constants.height
          : y - Constants.height;

    duplicates[2]
      ..x = duplicates[0].x
      ..y = duplicates[1].y;

    for (Bitmap dup in duplicates) {
      dup
        ..scaleX = scaleX
        ..scaleY = scaleY
        ..pivotX = pivotX
        ..pivotY = pivotY
        ..alpha = alpha
        ..rotation = rotation
        ..visible = visible;
    }
  }

  Bitmap getClosestToPoint(num x1, num y1) {
    var dup0 = duplicates[0];
    var dup1 = duplicates[1];
    var dup2 = duplicates[2];

    num dist = math.pow(x - x1, 2) + math.pow(y - y1, 2);
    num dist0 = math.pow(dup0.x - x1, 2) + math.pow(dup0.y - y1, 2);
    num dist1 = math.pow(dup1.x - x1, 2) + math.pow(dup1.y - y1, 2);
    num dist2 = math.pow(dup2.x - x1, 2) + math.pow(dup2.y - y1, 2);

    num minDist = math.min(dist, math.min(dist0, math.min(dist1, dist2)));

    if (minDist == dist) {
      return this;
    } else if (minDist == dist0) {
      return dup0;
    } else if (minDist == dist1) {
      return dup1;
    } else {
      return dup2;
    }
  }

  void setIndex(int index) {
    if (parent != null) {
      (parent as State).setChildIndex(this, index);

      for (Bitmap dup in duplicates) {
        (parent as State).setChildIndex(dup, index);
      }
    }
  }
}
