part of graviparty;

class RecolorableSprite extends Bitmap {
  final int color;

  RecolorableSprite(this.color, BitmapData bitmapData) {
    this.bitmapData = bitmapData;

    for (int x = 0; x < bitmapData.width; x++) {
      for (int y = 0; y < bitmapData.height; y++) {
        if (bitmapData.getPixel(x, y) > 0) bitmapData.setPixel(x, y, color);
      }
    }
  }
}
