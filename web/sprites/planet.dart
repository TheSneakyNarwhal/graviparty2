part of graviparty;

class Planet extends WrapSprite {
  static final BitmapData _defaultBitmapData = _getDefaultBitmapData();

  static const int minRadius = 20;
  static const int drawRadius = 50;
  static const int minDistanceBetweenPlanets = 60;
  static const int radiusVariation = 25;
  static const num minGravity = 30;

  num _radius;

  num get radius => _radius;

  final Player player;

  void set radius(num rad) {
    _radius = math.max(rad, minRadius);
    scaleX = _radius / drawRadius;
    scaleY = scaleX;
  }

  Planet(this.player, num radius, int color)
      : super(color, _defaultBitmapData.clone()) {
    this.radius = radius;
  }

  static BitmapData _getDefaultBitmapData() {
    var shape = new Shape()
      ..graphics.beginPath()
      ..graphics.circle(drawRadius, drawRadius, drawRadius)
      ..graphics.closePath()
      ..graphics.fillColor(Color.White)
      ..applyCache(0, 0, drawRadius * 2, drawRadius * 2);

    return new BitmapData.fromRenderTextureQuad(shape.cache);
  }
}
